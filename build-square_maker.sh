#!/bin/bash

CONTAINER=naturalis/museumapp_image_square_maker
TAG=$1

if [ -z "$TAG" ]; then
    TAG="latest"
fi

../_build.sh $CONTAINER $TAG Dockerfile-square_maker