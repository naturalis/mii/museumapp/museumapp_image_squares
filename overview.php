<?php

    // include_once("auth.php");
    include_once("class.imageSquaresNew.php");

    $dbFolder = getEnv("IMAGE_SQUARES_PATH");
    $dbBakFolder = getEnv("IMAGE_SQUARES_BAK_PATH");
    $baseUrlSquaredImages = getEnv("URL_SQUARES_IMAGE_ROOT");

    $m = new imageSquares;

    $m->setDatabaseFolder($dbFolder);
    $m->setBackupFolder($dbBakFolder);
    $m->initialize();

    if (isset($_POST) && isset($_POST["action"]) && $_POST["action"]=="reset" && isset($_POST["name"]))
    {
        $m->resetSavedScientificName($_POST["name"]);
    }

    $perpage=60;

    $offset=isset($_GET["offset"]) ? $_GET["offset"] : 0;

    if(isset($_POST["search"]))
    {
        $search = $_POST["search"];
        $m->setSearchTerm($_POST["search"]);
    }

    if($_GET["type"]=='ready')
    {
        $processed = $m->getProcessed($perpage,$offset);
        $processed_count = $m->getProcessedCount();        
    }
    else
    {
        $processed = $m->getSkipped($perpage,$offset);
        $processed_count = $m->getSkippedCount();                
    }

?>
<!DOCTYPE html>
<html>
<head>

<link rel="stylesheet" type="text/css" href="css/style.css" media="screen">
<link rel="stylesheet" type="text/css" media="screen" title="default" href="css/inline_templates.css" />

<script type="text/javascript" src="js/inline_templates.js"></script>
<script type="text/javascript" src="js/main.js"></script>

<script
  src="https://code.jquery.com/jquery-3.4.1.min.js"
  integrity="sha256-CSXorXvZcTkaix6Yvo6HppcZGetbYMGWSFlBw8HfCJo="
  crossorigin="anonymous"></script>
<script type="text/javascript">

</script>
</head>

<style>
#searchForm {
    display: inline-block;
}
</style>

<body>

<div class="panel thumbs" style="padding-bottom: 5px">
    <span class="title">
<?php
    if($_GET["type"]!='ready')
    {
        echo "Overzicht overgeslagen objecten ($processed_count)";
    }
    else
    {
        echo "Overzicht verwerkte objecten ($processed_count)";
    }
?>
    </span>

    <form id="searchForm" method="post" action="<?php echo $_SERVER['REQUEST_URI']?>">
        <input type="text" name="search" id="search" placeholder="(part of a) name" value="<?php echo $search ?? ""; ?>" />
        <input type="submit" value="zoek" />
    </form>


</div>

<table>
    <tr>
<?php
    
    $per_row=7;

// print_r($processed);die();

    foreach ((array)$processed as $key => $val)
    {
        echo sprintf('<td class="bordered topped %s"><table><tr><td class="%s" ondblclick="copyname(this);" colspan=2>%s</td></tr>',
            (!empty($val["filename_squared"]) ? "squared" : "" ),
            (!empty($val["filename_squared"]) ? "squared" : "unsquared" ),
            $val["scientific_name"]
        );
        echo '<tr><td class="topped">';

        if($_GET["type"]=='ready')
        {
            if (!empty($val["filename_squared"]))
            {
                echo sprintf('<img src="%s%s" class="thumb squared" onclick="popupimage(this)"><br />',$baseUrlSquaredImages,$val["filename_squared"]);
            }
            else
            {
                echo sprintf('<img src="%s" class="thumb" onclick="popupimage(this)"><br />',str_replace('/large', '/medium', $val["url"]));
            }
            
        }
        else
        {
            foreach ($val["urls"] as $url)
            {
                echo sprintf('<img src="%s" class="thumb" onclick="popupimage(this)"> ',str_replace('/large', '/small', $url));
            }
        }
        echo '</td><td style="vertical-align:top">';
   
        echo '</td></tr>';
        echo sprintf(
            '<tr><td colspan=2><a href="#" class="reset" onclick="$(\'#name\').val(\'%s\');$(\'#resetForm\').submit();">reset</a></td></tr>',
            addslashes($val["scientific_name"]));
        echo '</td></tr></table>';

        if (($key+1) % $per_row==0)
        {
            echo '</tr><tr>';
        }
    }

?>
    </tr>
</table>

<div class="paginator" id="paginator">
    pagina: 
<?php

    if ($_GET["offset"]>0)
    {
        echo sprintf(' <a href="?type=%s&offset=%s">&nbsp;&lt;&nbsp;</a> ',$_GET["type"],($_GET["offset"] - $perpage));
    }

    for ($i=0;$i<ceil($processed_count /  $perpage);$i++ )
    {
        if (($i * $perpage)==$_GET["offset"])
        {
            echo sprintf(' &nbsp;%s&nbsp; ',$i);
        }
        else
        {
            if (abs(($_GET["offset"] - ($i * $perpage))) < (4 * $perpage))
            {
                echo sprintf(' <a href="?type=%s&offset=%s">&nbsp;%s&nbsp;</a> ',$_GET["type"],($i * $perpage),$i);
            }
        }   
    }
    
    if (($_GET["offset"] + $perpage) < $processed_count)
    {
        echo sprintf(' <a href="?type=%s&offset=%s">&nbsp;&gt;&nbsp;</a> ',$_GET["type"],($_GET["offset"] + $perpage));
    }

?>
</div>

<form method="post" id="resetForm">
    <input type="hidden" name="action" value="reset">
    <input type="hidden" id="name" name="name" value="">
</form>


<div class="footer">
    Ga naar:
    <ul>
<?php
    if($_GET["type"]=='ready')
    {
        echo '<li><a href="overview.php?type=skipped">overzicht overgeslagen objecten</a></li>';
    }
    else
    {
        echo '<li><a href="overview.php?type=ready">overzicht verwerkte objecten</a></li>';
    }
?>

        <li><a href="index.php">nog te verwerken objecten</a></li>
    </ul>
</div>

</body>
