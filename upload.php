<?php

    // include_once("auth.php");
    include_once("class.imageSquaresNew.php");

    $dbFolder = $_ENV["IMAGE_SQUARES_PATH"];

    $db["host"] = isset($_ENV["MYSQL_HOST"]) ? $_ENV["MYSQL_HOST"] : 'mysql';
    $db["user"] = isset($_ENV["MYSQL_USER"]) ? $_ENV["MYSQL_USER"] : 'root';
    $db["pass"] = isset($_ENV["MYSQL_ROOT_PASSWORD"]) ? $_ENV["MYSQL_ROOT_PASSWORD"] : 'root';
    $db["database"] = isset($_ENV["MYSQL_DATABASE"]) ? $_ENV["MYSQL_DATABASE"] : 'reaper';

?>
<!DOCTYPE html>
<html>
<head>
	<link rel="stylesheet" type="text/css" href="style.css" media="screen">
</head>
<script
  src="https://code.jquery.com/jquery-3.4.1.min.js"
  integrity="sha256-CSXorXvZcTkaix6Yvo6HppcZGetbYMGWSFlBw8HfCJo="
  crossorigin="anonymous"></script>
<style type="text/css">
textarea {
	width: 1000px;
	height: 750px;
}
</style>
<body>
	<div>
		<form method="POST">
            <div>
    			<textarea id="newurls" name="newurls" placeholder="unitid[TAB]medialib URI
unitid[TAB]medialib URI
..."></textarea>
            </div>
            <div>
                <label><input type="checkbox" name="rescan" value="1">overwrite and re-scan already existing unitid/URL-sets</label>
            </div>
            <div>
            	<input type="submit" value="save">
            </div>
		</form>
		<div>
		  <a href="index.php">back</a>
        </div>
	</div>
<?php

    $m = new imageSquares;

	$m->setDatabaseCredentials($db);
    $m->setDatabaseFolder($dbFolder);
    $m->initialize();

	if (isset($_POST) && isset($_POST["newurls"]))
	{
		echo "<pre>";

		$p = explode(PHP_EOL,$_POST["newurls"]);

		$m->setDeleteAndRescan(isset($_POST["rescan"]) && $_POST["rescan"]=='1');
        $m->setDbTransaction();

		foreach ($p as $key => $line)
		{
			try
			{
				$values = explode(chr(9),$line);
				if (count($values)==2)
				{
					echo $m->saveUnitIDNameAndUrl($values[0],$values[1]), "\n";
				}
				else
				{
					echo "didn't save $line (too few or too many fields; check for trailing empty cells)", "\n";
				}
			}
			catch(Exception $e)
			{
				echo $e->getMessage(), "\n";
			}			
		}

		$m->setDbTransaction( true );

		echo "</pre>";
	}

?>	
</body>
</html>