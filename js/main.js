function copyname(element) {
    document.body.style.cursor = "progress";
    var $temp = $("<input>");
    $("body").append($temp);
    $temp.val($(element).text()).select();
    document.execCommand("copy");
    $temp.remove();
    setTimeout(function() {document.body.style.cursor = "default";},250);
}