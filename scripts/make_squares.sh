#!/bin/bash

NOW=$(date)

echo "$NOW making square species images"

DATABASE=$IMAGE_SQUARES_DB_PATH
DATABASE_COPY=${IMAGE_SQUARES_DB_PATH}-copy
OUTDIR=$IMAGE_SQUARED_IMAGES

echo "database: $DATABASE"
echo "outdir: $OUTDIR"

cp $DATABASE $DATABASE_COPY

sqlite3 $DATABASE_COPY "select _a.scientific_name, _a.url, _a.square_coordinates from squares_new _a left join squared_images_new _b on _a.scientific_name = _b.scientific_name where _b.scientific_name is null" | while read line; do
    IFS='|' read -ra FIELDS <<< "$line"
    ITER=0
    for i in "${FIELDS[@]}"; do

        if [ "$ITER" -eq "0" ]
        then
            NAME=$i
        fi

        if [ "$ITER" -eq "1" ]
        then
            URL=$i
        fi

        if [ "$ITER" -eq "2" ]
        then
            WIDTH=$(echo $i | jq '.width')
            X=$(echo $i | jq '.x1')
            Y=$(echo $i | jq '.y1')
            AUTO_ORIENT=$(echo $i | jq '.auto_orient')
        fi

        ITER=$(expr $ITER + 1)
    done

    if [ "$AUTO_ORIENT" == "true" ]
    then
        FLAG="-auto-orient"
    else
        FLAG=""
    fi

    # fix mds, feb 2022; apparently all browsers now behave the same
    FLAG="-auto-orient"

    MEDIALIB_POS=$(echo $URL | grep -ob "medialib")
    
    if [ ! -z "$MEDIALIB_POS" ]; then
        # https://medialib.naturalis.nl/file/id/RMNH.MAM.60006_4/format/large
        REG_NO=$(echo "$URL" | sed -e 's/^[^m]*medialib\.naturalis\.nl\/file\/id\///g' -e 's/\/format.*//g')
        URL=$(echo "$URL" | sed -e 's/http:/https:/g')
        URL_MASTER=$(echo "$URL" | sed -e 's/large/master/g')
        # calculating scaled values for master
        W_LARGE=$(convert $URL -format "%w" info:)
        W_MASTER=$(convert $URL_MASTER -format "%w" info:)
        SCALE=$(echo "$W_MASTER/$W_LARGE" | bc -l)
    else
        # https://pipeline-museumapp.naturalis.nl/leenobject_images/leen.808.RM.27.haai.jpg
        REG_NO=$(echo "$URL" | sed -E -e 's/(https|http):\/\/[^/]*\/leenobject_images\///g' -e 's/\.(jpg|JPG)//g')
        URL_MASTER=$URL
        SCALE=1
    fi

    FILENAME=${REG_NO//[ ]/_}_$(date +%s)_SQUARED.jpg
    FILENAME=${FILENAME//%/_}

    M_WIDTH=$(echo "result=$WIDTH*$SCALE;scale=0;result / 1" | bc -l)
    M_X=$(echo "result=$X*$SCALE;scale=0;result / 1" | bc -l)
    M_Y=$(echo "result=$Y*$SCALE;scale=0;result / 1" | bc -l)

    # can't get the https-policies of ImageMagick to work, hence the CURL-download
    CURL_DOWNLOAD=/tmp/curl-download

    if (( $(echo "$SCALE > 1" |bc -l) )); then
        echo "${NAME} --> ${FILENAME} (from master; ${SCALE})"
        curl -s -L $URL_MASTER --output $CURL_DOWNLOAD
        convert $CURL_DOWNLOAD ${FLAG} -crop ${M_WIDTH}x${M_WIDTH}+${M_X}+${M_Y} ${OUTDIR}${FILENAME}
    else
        echo "${NAME} --> ${FILENAME} (from large; ${SCALE})"
        curl -s -L $URL --output $CURL_DOWNLOAD
        convert $CURL_DOWNLOAD ${FLAG} -crop ${WIDTH}x${WIDTH}+${X}+${Y} ${OUTDIR}${FILENAME}
    fi

    echo "created ${OUTDIR}${FILENAME}"

    NAME=$(echo $NAME | sed "s/'/\'\'/")
    sqlite3 $DATABASE "insert into squared_images_new (scientific_name,filename) values ('$NAME','$FILENAME')"
done

rm $DATABASE_COPY

echo "done"

