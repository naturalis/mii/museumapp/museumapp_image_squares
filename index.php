<?php

/*
    1. Chrome landscpae
    2. FF portrait
    3. FF landscape
    4. Chrome portrair (gekanteld)
*/

    $dbFolder = $_ENV["IMAGE_SQUARES_PATH"];
    $dbBakFolder = $_ENV["IMAGE_SQUARES_BAK_PATH"];

#    include_once("auth.php");
    include_once("class.imageSquaresNew.php");

    $m = new imageSquares;

    $m->setDatabaseFolder($dbFolder);
    $m->setBackupFolder($dbBakFolder);
    $m->initialize();


    if (isset($_POST) && isset($_POST["action"]) && $_POST["action"]=="reset" && isset($_POST["name"]))
    {
        $m->resetSavedScientificName($_POST["name"]);
    }
    else
    if (isset($_POST) && isset($_POST["url"]) && isset($_POST["x1"]))
    {
        $m->saveSquare($_POST);
    }
    else
    if (isset($_POST) && isset($_POST["action"]) && $_POST["action"]=="skip")
    {
        $m->saveSkip($_POST["name"]);
        // $prev = $m->getSkippedUrls($_POST["name"]);
    }

    if (isset($_GET) && isset($_GET["name"]))
    {
        $batch = $m->getNextScientificName( $_GET["name"] );
    }
    else
    {
        $batch = $m->getNextScientificName();
    }
    $remain = $m->getRemainingScientificNameCount();

?>
<!DOCTYPE html>
<html>
<head>

<link rel="stylesheet" type="text/css" href="css/style.css" media="screen">
<link rel="stylesheet" type="text/css" media="screen" title="default" href="css/inline_templates.css" />

<script type="text/javascript" src="js/inline_templates.js"></script>
<script
  src="https://code.jquery.com/jquery-3.4.1.min.js"
  integrity="sha256-CSXorXvZcTkaix6Yvo6HppcZGetbYMGWSFlBw8HfCJo="
  crossorigin="anonymous"></script>
<script type="text/javascript" src="js/main.js"></script>

<link rel="stylesheet" type="text/css" href="js/jquery.imgareaselect-0.9.10/css/imgareaselect-default.css" />
<script type="text/javascript" src="js/jquery.imgareaselect-0.9.10/scripts/jquery.imgareaselect.pack.js"></script>
<script type="text/javascript">

var images=Array();
var currentUrl = "";
var currentUnitId=null;
var name = "";

function addImage( img, unitid )
{
    images.push( { url:img, unitid: unitid } );
}

function printImages()
{
    buffer = Array();
    for(var i=0;i<images.length;i++)
    {
        buffer.push(fetchTemplate( 'imageImageTpl' )
            .replace('%up%',fetchTemplate( 'imageIconUpTpl' ))
            .replace('%down%',fetchTemplate( 'imageIconDownTpl' ))
            .replace(/%src%/g,images[i].url)
            .replace(/%key%/g,images[i].unitid)
            .replace('%exclude%',fetchTemplate( 'overlayCrossTpl' ))
        );
    }

    $('div.images').html(fetchTemplate( 'imageTableTpl' ).replace('%body%',buffer.join("\n")));
}

function save()
{
    var form = $('<form action="" method="POST"></form>');

    form.appendTo('body')
    hiddenField.setAttribute("type", "hidden");
    form.append(hiddenField);

    form.submit();
}

function skip()
{
    var form = $('<form action="" method="POST"></form>');
    form.appendTo('body')

    var hiddenField = document.createElement("input");
    hiddenField.setAttribute("type", "hidden");
    hiddenField.setAttribute("name", "name");
    hiddenField.setAttribute("value", name );
    form.append(hiddenField);

    var hiddenField = document.createElement("input");
    hiddenField.setAttribute("type", "hidden");
    hiddenField.setAttribute("name", "action");
    hiddenField.setAttribute("value", "skip" );
    form.append(hiddenField);

    form.submit();
}

function selectImage(ele)
{
    var h = $(ele).height();
    var w = $(ele).width();
    var f = 3;

    var hacc = h *f;
    var wacc = w *f;

    currentUrl = $(ele).attr('src');
    currentUnitId = $(ele).attr('data-key');

    //$('#selected').attr('src',currentUrl).css('width',wacc+'px').css('height',hacc+'px');
    $('#selected').attr('src',currentUrl);

    $('img#selected').imgAreaSelect({
        aspectRatio: '1:1', 
        handles: true,
        onSelectEnd: onSelectEnd,
        onSelectChange: onSelectChange
    });

}

function onSelectEnd(img, selection)
{
    $('input[name="url"]').val(currentUrl);
    $('input[name="unitId"]').val(currentUnitId);
    $('input[name="x1"]').val(selection.x1);
    $('input[name="y1"]').val(selection.y1);
    $('input[name="x2"]').val(selection.x2);
    $('input[name="y2"]').val(selection.y2);
    $('input[name="w"]').val($('#selected').width());
    $('input[name="h"]').val($('#selected').height());
    $('input[name="name"]').val(name);
    $('input[name="isFirefox"]').val(navigator.userAgent.toLowerCase().indexOf('firefox') > -1 ? "1" : "0");
    $('input[name="submit"]').toggle(true);

    onSelectChange(img, selection);
}

function onSelectChange(img, selection)
{
    if (selection.width==0 || selection.height==0)
    {
        $('input[name="submit"]').toggle(false);
    }
}

function doGoogle()
{
    str = $('#taxon_name').html();
    url = "https://www.google.com/search?q=" + encodeURIComponent(str);
    window.open(url,'_new');
}

</script>
</head>
<body>

<?php
    echo sprintf('<span onclick="doGoogle();" class="title" style="cursor:pointer;" title="lookup in google"><span id="taxon_name">%s</span> [%s]</span>',$batch[0]["scientific_name"],$remain);
    echo '<span onclick="copyname(\'#taxon_name\');" style="margin-left:5px; cursor:pointer;" title="copy name">&#128203;</span>';
    echo '<input style="margin-left:10px" type="button" name="save" value="skip" onclick="skip();">';

?>
<table class="structure">

    <tr>
        <td>
            <div class="panel images">
            </div>
        </td>
        <td>
            <div id="imgContainer">
                <img src="" id="selected">
                <input type="button" name="submit" value="save" onclick="$('#theForm').submit();" style="display: none;vertical-align: top" />
            </div>
        </td>
    </tr>
</table>

<div class="footer">
    Ga naar:
    <ul>
        <li><a href="overview.php?type=ready">overzicht verwerkte objecten</a></li>
        <li><a href="overview.php?type=skipped">overzicht overgeslagen objecten</a></li>
    </ul>
</div>

<form id="theForm" action="" method="post">
    <input type="hidden" name="url" value="" />
    <input type="hidden" name="name" value="" />
    <input type="hidden" name="unitId" value="" />
    <input type="hidden" name="isFirefox" value="" />
    <input type="hidden" name="x1" value="" />
    <input type="hidden" name="y1" value="" />
    <input type="hidden" name="x2" value="" />
    <input type="hidden" name="y2" value="" />
    <input type="hidden" name="h" value="" />
    <input type="hidden" name="w" value="" />
</form>

<script type="text/javascript">
$(document).ready(function()
{
    $.fn.classList = function() {return this[0].className.split(/\s+/);};

    acquireInlineTemplates();

<?php
    echo "name = '".addslashes($batch[0]["scientific_name"])."';\n";
    foreach ($batch as $key => $val)
    {
        echo "addImage('".$val['url']."','".$val['unitid']."');\n";
    }
?>
    printImages();

});

</script>

<div class="inline-templates" id="imageTableTpl">
<!--
    <table>
        %body%
    </table>
-->
</div>

<div class="inline-templates" id="imageImageTpl">
<!--
    <tr data-key="%key%">
        <td class="medialib divider">
            <img onclick="selectImage(this);" data-key="%key%" src="%src%" class="medialib" title="%src%"/><br />
            %key%
        </td>
    </tr>
-->
</div>

</body>
</html>
